import general.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;

public class ChessController {
    final private Board model;
    final private ChessView view;
    final private History history;
    final private ArrayList<Coordinate> moveHolder = new ArrayList<>();
    final private Player playerWhite;
    final private Player playerBlack;
    private ActionListener actionListener;
    private Piece.PieceColor whosTurn = Piece.PieceColor.WHITE;

    /**
     * Constructor for the chess controller
     *
     * @param model the model to control
     * @param view  the view to control
     */
    public ChessController(Board model, ChessView view, History history, Player playerWhite, Player playerBlack) {
        this.model = model;
        this.view = view;
        this.history = history;
        this.playerWhite = playerWhite;
        this.playerBlack = playerBlack;
    }

    /**
     * This is the game loop where controls are looped to check the game status
     *
     * @throws IOException if there is a problem getting the chess piece images
     */
    public void control() throws IOException {
        pieceControl();
        specialControl();
        String whiteName = view.showInputBox("What's white player's name");
        String blackName = view.showInputBox("What's black player's name");

        playerWhite.setName(whiteName);
        playerBlack.setName(blackName);
    }

    /**
     * Controls chess pieces depending on user input
     */
    private void pieceControl() {
        actionListener = actionEvent -> {
            for (int i = 0; i < view.getButtonGrid().length; i++) {
                for (int j = 0; j < view.getButtonGrid()[0].length; j++) {
                    if (actionEvent.getSource() == view.getButtonGrid()[i][j]) {
                        try {
                            moveHolderHelper(new Coordinate(i, j));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        };

        //Setting up click listener
        for (int row = 0; row < view.getButtonGrid().length; row++) {
            for (int col = 0; col < view.getButtonGrid()[0].length; col++) {
                view.getButtonGrid()[row][col].addActionListener(actionListener);
            }
        }
    }

    /**
     * Controls special buttons under the menu depending on user input
     */
    private void specialControl() {
        Hashtable<String, JMenuItem> table = view.getSpecialButtons();

        //Set up actions depending on different special control
        actionListener = actionEvent -> {
            if (actionEvent.getSource() == table.get("Undo")) {
                try {
                    undoMove();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (actionEvent.getSource() == table.get("Forfeit")) {
                try {
                    forfeit();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (actionEvent.getSource() == table.get("Score")) {
                showScore();
            } else if (actionEvent.getSource() == table.get("Tie")) {
                try {
                    askTie();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (actionEvent.getSource() == table.get("Reset")) {
                resetGame();
                try {
                    view.showPieces(model.getAllPiece());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };

        //Setting up click listener
        for (String key : table.keySet()) {
            table.get(key).addActionListener(actionListener);
        }
    }

    /**
     * When a player asks for a Tie, if the opponent types in "YES" the game resets
     * This will not add points to the players
     * @throws IOException
     */
    private void askTie() throws IOException {
        String tieAccepted;
        if (whosTurn == Piece.PieceColor.WHITE) {
            tieAccepted = view.showInputBox(playerBlack.getName() + ", Would you like Tie?");
        } else {
            tieAccepted = view.showInputBox(playerWhite.getName() + ", Would you like Tie?");
        }

        if (tieAccepted.equals("Yes")) {
            resetGame();
            view.showPieces(model.getAllPiece());
        }
    }

    /**
     * Shows the current score board
     */
    private void showScore() {
        view.alertBox(playerWhite.getName() + ": " + playerWhite.getNumOfWins() + ", " +
                playerBlack.getName() + ": " + playerBlack.getNumOfWins());
    }

    /**
     * When a player forfeits, the opponents are granted one win
     * @throws IOException when there is a problem getting the chess images
     */
    private void forfeit() throws IOException {
        resetGame();
        if (whosTurn == Piece.PieceColor.BLACK) {
            playerWhite.setNumOfWins(playerWhite.getNumOfWins() + 1);
        } else {
            playerBlack.setNumOfWins(playerBlack.getNumOfWins() + 1);
        }
        view.showPieces(model.getAllPiece());
    }

    /**
     * Using the history, the board will revert to the previous status
     * @throws IOException
     */
    private void undoMove() throws IOException {
        if (history.numOfMoves() > 1) {
            model.setPieces(history.getHistory());
            changeWhosTurn(whosTurn);
        } else {
            resetGame();
        }
        view.showPieces(model.getAllPiece());
    }

    /**
     * Helper function to return the board to starting state
     */
    private void resetGame() {
        history.clear();
        model.setPiecesOnRectangleBoard();
        changeWhosTurn(Piece.PieceColor.BLACK);
    }

    /**
     * Helper function to move pieces
     * This is going to color the clicked piece and show possible position by colors
     * After the move, check the game condition
     * @param coordinate the coordinate on board GUI where user clicked
     * @throws IOException when there is a problem getting the chess images
     */
    private void moveHolderHelper(Coordinate coordinate) throws IOException {
        Piece piece = model.getPiece(coordinate);

        //Selecting a piece to move
        if (piece != null && piece.getColor() == whosTurn && moveHolder.size() == 0) {
            view.getButtonGrid()[coordinate.getRow()][coordinate.getColumn()].setBackground(Color.ORANGE);
            ArrayList<Coordinate> possiblePositions = piece.possibleMoves(model);

            //Color all possible destination of a chosen piece
            for (Coordinate c : possiblePositions) {
                history.addHistory(model.getAllPiece());
                piece = model.getPiece(coordinate);
                boolean moveSuccess = piece.makeMove(model, history, c);
                if (moveSuccess) {
                    view.getButtonGrid()[c.getRow()][c.getColumn()].setBackground(Color.ORANGE);
                    history.removeHistory();
                }
                model.setPieces(history.getHistory());
            }
            moveHolder.add(coordinate);
        }

        //When a valid piece is selected and final destination is selected
        else if (moveHolder.size() == 1) {
            piece = model.getPiece(moveHolder.get(0));
            boolean moveSuccess = piece.makeMove(model, history, coordinate);
            if (moveSuccess) {
                changeWhosTurn(whosTurn);
                moveHolder.clear();
                view.showPieces(model.getAllPiece());
            } else {
                moveHolder.clear();
                view.showPieces(model.getAllPiece());
            }
            checkGameStatus();
        }
    }

    /**
     * Checks the current status of the board and notifies players
     * whether check, checkmate, or stalemate occured
     * @throws IOException
     */
    private void checkGameStatus() throws IOException {
        //Check game condition
        if (model.isInCheck() != null) {
            //mark red spot if a king is in check
            Coordinate kingPosition = model.isInCheck();
            view.getButtonGrid()[kingPosition.getRow()][kingPosition.getColumn()].setBackground(Color.RED);

            //when the game is in checkmate
            if (model.isCheckmate(model.isInCheck(), history)) {
                if (model.getPiece(kingPosition).getColor() == Piece.PieceColor.BLACK) {
                    playerWhite.setNumOfWins(playerWhite.getNumOfWins() + 1);
                } else {
                    playerBlack.setNumOfWins(playerBlack.getNumOfWins() + 1);
                }
                view.alertBox("Game Over");
            } else {
                view.alertBox("Is in Check");
            }
        } else {
            if (model.isStalemate(whosTurn, history)) {
                view.alertBox("Stalemate");
                view.showPieces(model.getAllPiece());
            }
        }
    }

    /**
     * Changes the current turn to the opposite player
     * @param color the current player color
     */
    private void changeWhosTurn(Piece.PieceColor color) {
        whosTurn = color.oppositeColor();
        view.setTitle(String.valueOf(whosTurn));
    }
}
