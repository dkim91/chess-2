import general.Board;
import general.History;
import general.Piece;
import general.Player;

import java.io.IOException;

public class MVCRunner {
    private final static int BOARD_ROWS = 8;
    private final static int BOARD_COLUMNS = 8;

    public static void main(String[] args) throws IOException {
        //Setting up Model
        Board model = retrievePieceFromDatabase();
        ChessView view = new ChessView(model.getVerticalSize(), model.getVerticalSize());
        History history = new History();
        Player playerWhite = new Player("", Piece.PieceColor.WHITE);
        Player playerBlack = new Player("", Piece.PieceColor.BLACK);

        //View
        view.showPieces(model.getAllPiece());

        //Controller
        ChessController controller = new ChessController(model, view, history, playerWhite, playerBlack);
        controller.control();
    }

    /**
     * Helper function to get board model. Acting as a model
     *
     * @return the current game board
     */
    private static Board retrievePieceFromDatabase() {
        Board board = new Board(BOARD_ROWS, BOARD_COLUMNS);
        board.setPiecesOnRectangleBoard();
        return board;
    }
}
