import general.Board;
import general.Coordinate;
import general.History;
import general.Piece;

import java.util.Scanner;

/**
 * This is a class for testing console style chess
 */
class Main {

    public static void main(String[] args) {
        Board board = new Board();
        History history = new History();
        board.setPiecesOnRectangleBoard();
        Piece.PieceColor whosTurn = Piece.PieceColor.WHITE;
        while (!board.gameOver()) {
            System.out.println("history size: " + history.numOfMoves());
            //Clear screen and show current board
            for (int i = 0; i < 50; ++i) System.out.println();
            board.consolePrint();

            //Check for check
            if (board.isInCheck() != null) {
                //If checkmate end the game, continue otherwise
                if (board.isCheckmate(board.isInCheck(), history)) {
                    System.out.println("Checkmate");
                    return;
                } else {
                    System.out.println("Is in Check");
                }
            } else {
                //Check for stalemate
                if (board.isStalemate(whosTurn, history)) {
                    System.out.println("Stalemate");
                    return;
                }
            }

            System.out.println(whosTurn + "'s turn");

            //Read user input
            Scanner sc = new Scanner(System.in);
            int originalRow = sc.nextInt();
            int originalCol = sc.nextInt();
            int destinationRow = sc.nextInt();
            int destinationCol = sc.nextInt();

            if (originalRow == -1) {
                System.out.println(history.numOfMoves());
                if (history.numOfMoves() > 1) {
                    board.setPieces(history.getHistory());
                    whosTurn = whosTurn.oppositeColor();
                } else {
                    history.clear();
                    board.setPiecesOnRectangleBoard();
                    whosTurn = Piece.PieceColor.WHITE;
                }
            } else {
                Piece p = board.getPiece(new Coordinate(originalRow, originalCol));
                if (p != null && p.getColor() == whosTurn) {
                    history.addHistory(board.getAllPiece());
                    boolean moveSuccess = p.makeMove(board, history, new Coordinate(destinationRow, destinationCol));
                    if (moveSuccess) {
                        whosTurn = whosTurn.oppositeColor();
                    } else {
                        history.removeHistory();
                    }
                }
            }
        }

    }
}
