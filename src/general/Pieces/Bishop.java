package general.Pieces;

import general.Board;
import general.Coordinate;
import general.Piece;

import java.util.ArrayList;

public class Bishop extends Piece {

    public Bishop(Piece.PieceColor color, Coordinate coordinate) {
        super();
        this.color = color;
        this.coordinate = coordinate;
    }

    /**
     * Given a piece return appropriate moves
     *
     * @param board the board to move pieces around
     * @return the valid arraylist of coordinates
     */
    @Override
    public ArrayList<Coordinate> possibleMoves(Board board) {
        int cRow = this.coordinate.getRow();
        int cCol = this.coordinate.getColumn();
        ArrayList<Coordinate> possiblePositions = new ArrayList<>();

        //Up-right
        int rowToCheck = cRow;
        int colToCheck = cCol;
        while (rowToCheck >= 0 && colToCheck < board.getHorizontalSize()) {
            rowToCheck--;
            colToCheck++;
            if (movement(board, possiblePositions, rowToCheck, colToCheck)) break;
        }

        //Up-left
        rowToCheck = cRow;
        colToCheck = cCol;
        while (rowToCheck >= 0 && colToCheck >= 0) {
            rowToCheck--;
            colToCheck--;
            if (movement(board, possiblePositions, rowToCheck, colToCheck)) break;
        }

        //Down-right
        rowToCheck = cRow;
        colToCheck = cCol;
        while (rowToCheck < board.getVerticalSize() && colToCheck < board.getHorizontalSize()) {
            rowToCheck++;
            colToCheck++;
            if (movement(board, possiblePositions, rowToCheck, colToCheck)) break;
        }

        //Down-left
        rowToCheck = cRow;
        colToCheck = cCol;
        while (rowToCheck < board.getVerticalSize() && colToCheck >= 0) {
            rowToCheck++;
            colToCheck--;
            if (movement(board, possiblePositions, rowToCheck, colToCheck)) break;
        }

        return possiblePositions;
    }

    /**
     * Helper function for possibleMoves
     *
     * @param board             the board to move a piece
     * @param possiblePositions the arraylist to add new possible positions to
     * @param rowToCheck        the destination row to check
     * @param colToCheck        the destination column to check
     * @return true if it is a possible move, false otherwise
     */
    private boolean movement(Board board, ArrayList<Coordinate> possiblePositions, int rowToCheck, int colToCheck) {
        Coordinate destToCheck = new Coordinate(rowToCheck, colToCheck);
        //If the destination is out of board or has same color
        if (isMoveToSameColor(board, destToCheck) || !board.isOnBoard(destToCheck)) {
            return true;
        } else {
            //If the destination has enemy color, last one to be added
            if (isMoveToEnemyColor(board, destToCheck)) {
                possiblePositions.add(destToCheck);
                return true;
            } else {
                possiblePositions.add(destToCheck);
            }
        }
        return false;
    }
}
