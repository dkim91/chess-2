package general.Pieces;

import general.Board;
import general.Coordinate;
import general.Piece;

import java.util.ArrayList;

public class Knight extends Piece {

    public Knight(Piece.PieceColor color, Coordinate coordinate) {
        super();
        this.color = color;
        this.coordinate = coordinate;
    }

    /**
     * Given a piece return appropriate moves
     *
     * @param board the board to move pieces around
     * @return the valid arraylist of coordinates
     */
    @Override
    public ArrayList<Coordinate> possibleMoves(Board board) {
        int cRow = this.coordinate.getRow();
        int cCol = this.coordinate.getColumn();
        ArrayList<Coordinate> possiblePositions = new ArrayList<>();

        /**
         * There are eight possible moves for a knight
         * For each top, left, right, bottom there are two spots
         */
        //Top
        if (knightJumpMove(board, new Coordinate(cRow - 2, cCol - 1))) {
            possiblePositions.add(new Coordinate(cRow - 2, cCol - 1));
        }
        if (knightJumpMove(board, new Coordinate(cRow - 2, cCol + 1))) {
            possiblePositions.add(new Coordinate(cRow - 2, cCol + 1));
        }

        //Left
        if (knightJumpMove(board, new Coordinate(cRow - 1, cCol - 2))) {
            possiblePositions.add(new Coordinate(cRow - 1, cCol - 2));
        }
        if (knightJumpMove(board, new Coordinate(cRow + 1, cCol - 2))) {
            possiblePositions.add(new Coordinate(cRow + 1, cCol - 2));
        }

        //Right
        if (knightJumpMove(board, new Coordinate(cRow - 1, cCol + 2))) {
            possiblePositions.add(new Coordinate(cRow - 1, cCol + 2));
        }
        if (knightJumpMove(board, new Coordinate(cRow + 1, cCol + 2))) {
            possiblePositions.add(new Coordinate(cRow + 1, cCol + 2));
        }

        //Bottom
        if (knightJumpMove(board, new Coordinate(cRow + 2, cCol - 1))) {
            possiblePositions.add(new Coordinate(cRow + 2, cCol - 1));
        }
        if (knightJumpMove(board, new Coordinate(cRow + 2, cCol + 1))) {
            possiblePositions.add(new Coordinate(cRow + 2, cCol + 1));
        }

        return possiblePositions;
    }

    private boolean knightJumpMove(Board board, Coordinate destCoordinate) {
        return canMoveToEmpty(board, destCoordinate) || isMoveToEnemyColor(board, destCoordinate);
    }
}
