package general.Pieces;

import general.Board;
import general.Coordinate;
import general.Piece;

import java.util.ArrayList;

public class Pawn extends Piece {

    public Pawn(PieceColor color, Coordinate coordinate) {
        super();
        this.color = color;
        this.coordinate = coordinate;
    }

    /**
     * Given a piece return appropriate moves
     *
     * @param board the board to move pieces around
     * @return the valid arraylist of coordinates
     */
    @Override
    public ArrayList<Coordinate> possibleMoves(Board board) {
        int cRow = this.coordinate.getRow();
        int cCol = this.coordinate.getColumn();
        ArrayList<Coordinate> possiblePositions = new ArrayList<>();

        //White pawn
        if (color == Piece.PieceColor.WHITE) {
            if (cRow == 6) { // Initial condition allows pawn to move 2 spaces
                if (canMoveToEmpty(board, new Coordinate(cRow - 1, cCol))) {
                    possiblePositions.add(new Coordinate(cRow - 1, cCol));
                    if (canMoveToEmpty(board, new Coordinate(cRow - 2, cCol))) {
                        possiblePositions.add(new Coordinate(cRow - 2, cCol));
                    }
                }
            } else {
                if (canMoveToEmpty(board, new Coordinate(cRow - 1, cCol))) {
                    possiblePositions.add(new Coordinate(cRow - 1, cCol));
                }
            }

            // Diagonal check
            // Pawn can move diagonally only if the destination is on the board
            // and destination has enemyColor
            //Up right
            if (isMoveToEnemyColor(board, new Coordinate(cRow - 1, cCol + 1))) {
                possiblePositions.add(new Coordinate(cRow - 1, cCol + 1));
            }

            //Up left
            if (isMoveToEnemyColor(board, new Coordinate(cRow - 1, cCol - 1))) {
                possiblePositions.add(new Coordinate(cRow - 1, cCol - 1));
            }
        } else { //Black pawn
            if (cRow == 1) { // Initial condition allows pawn to move 2 spaces
                if (canMoveToEmpty(board, new Coordinate(cRow + 1, cCol))) {
                    possiblePositions.add(new Coordinate(cRow + 1, cCol));
                    if (canMoveToEmpty(board, new Coordinate(cRow + 2, cCol))) {
                        possiblePositions.add(new Coordinate(cRow + 2, cCol));
                    }
                }
            } else {
                if (canMoveToEmpty(board, new Coordinate(cRow + 1, cCol))) {
                    possiblePositions.add(new Coordinate(cRow + 1, cCol));
                }
            }

            /*Diagonal check
            //Pawn can move diagonally only if the destination is on the board
            //and destination has enemyColor
            */
            //Down right
            if (isMoveToEnemyColor(board, new Coordinate(cRow + 1, cCol + 1))) {
                possiblePositions.add(new Coordinate(cRow + 1, cCol + 1));
            }

            //Down left
            if (isMoveToEnemyColor(board, new Coordinate(cRow + 1, cCol - 1))) {
                possiblePositions.add(new Coordinate(cRow + 1, cCol - 1));
            }
        }
        return possiblePositions;
    }

}
