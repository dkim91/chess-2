package general;

public class Player {
    private String name;
    private int numOfWins;
    private Piece.PieceColor color;

    public Player(String name, Piece.PieceColor color) {
        if (name == null || name.equals("")) {
            this.name = color.toString();
        } else {
            this.name = name;
        }
        this.numOfWins = 0;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumOfWins() {
        return numOfWins;
    }

    public void setNumOfWins(int numOfWins) {
        this.numOfWins = numOfWins;
    }
}
