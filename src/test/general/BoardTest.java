package general;

import general.Pieces.Rook;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class BoardTest {

    @Test
    public void testGetHorizontalSize() throws Exception {
        Board board = new Board();
        assertEquals(8, board.getHorizontalSize());
    }

    @Test
    public void testGetVerticalSize() throws Exception {
        Board board = new Board();
        assertEquals(8, board.getVerticalSize());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBoardConstructorInvalidRowArgument() throws Exception {
        Board board = new Board(-1, 8);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBoardConstructorInvalidColArgument() throws Exception {
        Board board = new Board(8, -1);
    }

    @Test
    public void testSetPiecesOnRectangleBoard() throws Exception {
        Board board = new Board();
        board.setPiecesOnRectangleBoard();
        assertEquals(Rook.class, board.getPiece(new Coordinate(0, 0)).getClass());
    }

    @Test
    public void testGetPiece() throws Exception {
        Board board = new Board();
        board.setPiecesOnRectangleBoard();
        Piece piece = board.getPiece(new Coordinate(0, 0));
        assertEquals(Piece.PieceColor.BLACK, piece.getColor());
    }

    @Test
    public void testRemovePiece() throws Exception {
        Board board = new Board(9, 9);
        History history = new History();
        board.setPiecesOnRectangleBoard();
        board.removePiece(new Coordinate(0, 0));
        assertEquals(null, board.getPiece(new Coordinate(0, 0)));
    }

    @Test
    public void testGetAllPiece() throws Exception {
        Board board = new Board();
        board.setPiecesOnRectangleBoard();
        ArrayList<Piece> allPieces = board.getAllPiece();
        assertEquals(32, allPieces.size());
    }

    @Test
    public void testGameNotOver() throws Exception {
        Board board = new Board();
        board.setPiecesOnRectangleBoard();
        boolean isGameOver = board.gameOver();
        assertEquals(false, isGameOver);
    }

    @Test
    public void testGameOver() throws Exception {
        Board board = new Board();
        History history = new History();
        board.setPiecesOnRectangleBoard();
        board.removePiece(new Coordinate(0, 4));
        board.removePiece(new Coordinate(board.getVerticalSize() - 1, 4));
        boolean isGameOver = board.gameOver();
        assertEquals(true, isGameOver);
    }

    @Test
    public void testIsInCheck() throws Exception {
        Board board = new Board();
        History history = new History();
        board.setPiecesOnRectangleBoard();

        //Move the pieces according to the website
        Piece p = board.getPiece(new Coordinate(6, 4));
        p.makeMove(board, history, new Coordinate(4, 4));

        p = board.getPiece(new Coordinate(1, 3));
        p.makeMove(board, history, new Coordinate(3, 3));

        p = board.getPiece(new Coordinate(4, 4));
        p.makeMove(board, history, new Coordinate(3, 3));

        p = board.getPiece(new Coordinate(0, 3));
        p.makeMove(board, history, new Coordinate(3, 3));

        p = board.getPiece(new Coordinate(6, 0));
        p.makeMove(board, history, new Coordinate(5, 0));

        p = board.getPiece(new Coordinate(3, 3));
        p.makeMove(board, history, new Coordinate(3, 4));

        Coordinate kingInCheck = board.isInCheck();
        assertEquals(7, kingInCheck.getRow());
        assertEquals(4, kingInCheck.getColumn());
        assertEquals(false, board.isCheckmate(kingInCheck, history));
    }

    /**
     * Check for the shortest checkmate
     * http://chess.about.com/od/tipsforbeginners/ss/Foolsmate.htm
     *
     * @throws Exception
     */
    @Test
    public void testIsInCheckmateTwo() throws Exception {
        Board board = new Board();
        History history = new History();
        board.setPiecesOnRectangleBoard();

        //Move the pieces according to the website
        Piece p = board.getPiece(new Coordinate(6, 5));
        p.makeMove(board, history, new Coordinate(5, 5));

        p = board.getPiece(new Coordinate(1, 4));
        p.makeMove(board, history, new Coordinate(3, 4));

        p = board.getPiece(new Coordinate(6, 6));
        p.makeMove(board, history, new Coordinate(4, 6));

        p = board.getPiece(new Coordinate(0, 3));
        p.makeMove(board, history, new Coordinate(4, 7));

        Coordinate kingInCheck = board.isInCheck();
        assertEquals(7, kingInCheck.getRow());
        assertEquals(4, kingInCheck.getColumn());
    }

    /**
     * Check for the shortest checkmate
     * http://chess.about.com/od/tipsforbeginners/ss/Foolsmate.htm
     *
     * @throws Exception
     */
    @Test
    public void testIsCheckmateOne() throws Exception {
        Board board = new Board();
        History history = new History();
        board.setPiecesOnRectangleBoard();

        //Move the pieces according to the website
        Piece p = board.getPiece(new Coordinate(6, 5));
        p.makeMove(board, history, new Coordinate(5, 5));

        p = board.getPiece(new Coordinate(1, 4));
        p.makeMove(board, history, new Coordinate(3, 4));

        p = board.getPiece(new Coordinate(6, 6));
        p.makeMove(board, history, new Coordinate(4, 6));

        p = board.getPiece(new Coordinate(0, 3));
        p.makeMove(board, history, new Coordinate(4, 7));

        Coordinate kingInCheck = board.isInCheck();
        assertEquals(true, board.isCheckmate(kingInCheck, history));
    }

    /**
     * Check for a stalemate scenario
     * http://www.chess.com/forum/view/fun-with-chess/fastest-stalemate
     *
     * @throws Exception
     */
    @Test
    public void testIsStalemate() throws Exception {
        Board board = new Board();
        History history = new History();
        board.setPiecesOnRectangleBoard();

        //Move the pieces according to the website
        Piece p = board.getPiece(new Coordinate(6, 2));
        p.makeMove(board, history, new Coordinate(4, 2));

        p = board.getPiece(new Coordinate(1, 7));
        p.makeMove(board, history, new Coordinate(3, 7));

        p = board.getPiece(new Coordinate(6, 7));
        p.makeMove(board, history, new Coordinate(4, 7));

        p = board.getPiece(new Coordinate(1, 0));
        p.makeMove(board, history, new Coordinate(3, 0));

        p = board.getPiece(new Coordinate(7, 3));
        p.makeMove(board, history, new Coordinate(4, 0));

        p = board.getPiece(new Coordinate(0, 0));
        p.makeMove(board, history, new Coordinate(2, 0));

        p = board.getPiece(new Coordinate(4, 0));
        p.makeMove(board, history, new Coordinate(3, 0));

        p = board.getPiece(new Coordinate(2, 0));
        p.makeMove(board, history, new Coordinate(2, 7));

        p = board.getPiece(new Coordinate(3, 0));
        p.makeMove(board, history, new Coordinate(1, 2));

        p = board.getPiece(new Coordinate(1, 5));
        p.makeMove(board, history, new Coordinate(2, 5));

        p = board.getPiece(new Coordinate(1, 2));
        p.makeMove(board, history, new Coordinate(1, 3));

        p = board.getPiece(new Coordinate(0, 4));
        p.makeMove(board, history, new Coordinate(1, 5));

        p = board.getPiece(new Coordinate(1, 3));
        p.makeMove(board, history, new Coordinate(1, 1));

        p = board.getPiece(new Coordinate(0, 3));
        p.makeMove(board, history, new Coordinate(5, 3));

        p = board.getPiece(new Coordinate(1, 1));
        p.makeMove(board, history, new Coordinate(0, 1));

        p = board.getPiece(new Coordinate(5, 3));
        p.makeMove(board, history, new Coordinate(1, 7));

        p = board.getPiece(new Coordinate(0, 1));
        p.makeMove(board, history, new Coordinate(0, 2));

        p = board.getPiece(new Coordinate(1, 5));
        p.makeMove(board, history, new Coordinate(2, 6));

        p = board.getPiece(new Coordinate(0, 2));
        p.makeMove(board, history, new Coordinate(2, 4));
        assertEquals(true, board.isStalemate(Piece.PieceColor.BLACK, history));
    }

    @Test
    public void testIsOnBoard() throws Exception {
        Board board = new Board();
        int horizontalSize = board.getHorizontalSize();
        int verticalSize = board.getVerticalSize();
        assertEquals(false, board.isOnBoard(new Coordinate(horizontalSize + 2, verticalSize + 2)));
    }
}