package general;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CoordinateTest {

    @Test
    public void testGetRow() throws Exception {
        Coordinate xy = new Coordinate(3, 34);
        assertEquals(3, xy.getRow());
    }

    @Test
    public void testGetColumn() throws Exception {
        Coordinate xy = new Coordinate(3, 34);
        assertEquals(34, xy.getColumn());
    }
}