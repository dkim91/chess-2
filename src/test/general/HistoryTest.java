package general;

import general.Pieces.Pawn;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HistoryTest {

    @Test
    public void testAddHistory() throws Exception {
        Board board = new Board();
        History history = new History();
        board.setPiecesOnRectangleBoard();

        //Move the pieces according to the website
        Piece p = board.getPiece(new Coordinate(6, 0));
        p.makeMove(board, history, new Coordinate(4, 0));
        assertEquals(1, history.numOfMoves());
    }

    @Test
    public void testGetHistory() throws Exception {
        Board board = new Board();
        History history = new History();
        board.setPiecesOnRectangleBoard();

        //Move the pieces according to the website
        Piece p = board.getPiece(new Coordinate(6, 0));
        p.makeMove(board, history, new Coordinate(4, 0));
        board.setPieces(history.getHistory());

        Piece pawn = new Pawn(Piece.PieceColor.BLACK, new Coordinate(-1, -1));
        assertEquals(board.getPiece(new Coordinate(6, 0)).getClass(), pawn.getClass());
    }

}