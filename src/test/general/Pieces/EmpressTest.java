package general.Pieces;

import general.Board;
import general.Coordinate;
import general.Piece;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class EmpressTest {
    /**
     * Test possible moves of a empress in place of a rook on the original starting board
     *
     * @throws Exception
     */
    @Test
    public void testPossibleEmpressMoves() throws Exception {
        Coordinate rookPosition = new Coordinate(7, 0);

        //Set up a board with a special piece
        Board board = new Board();
        board.setPiecesOnRectangleBoard();
        board.removePiece(rookPosition);
        ArrayList<Piece> allPieces = board.getAllPiece();
        allPieces.add(new Empress(Piece.PieceColor.WHITE, rookPosition));

        //Test for possible moves
        Piece empress = board.getPiece(rookPosition);
        ArrayList<Coordinate> al = empress.possibleMoves(board);
        assertEquals(1, al.size());
    }

    /**
     * Test possible moves of a empress positioned in the center on original board
     *
     * @throws Exception
     */
    @Test
    public void testPossibleEmpressMovesCenterBoard() throws Exception {
        Coordinate rookPosition = new Coordinate(7, 0);

        //Set up a board with a special piece
        Board board = new Board();
        board.setPiecesOnRectangleBoard();
        board.removePiece(rookPosition);
        ArrayList<Piece> allPieces = board.getAllPiece();
        allPieces.add(new Empress(Piece.PieceColor.WHITE, rookPosition));

        //Test for possible moves
        Piece empress = board.getPiece(rookPosition);
        empress.setCoordinate(new Coordinate(3, 3));
        ArrayList<Coordinate> al = empress.possibleMoves(board);
        assertEquals(19, al.size());
    }

    /**
     * Test possible moves of an empress positioned in the bishop position on an empty board
     *
     * @throws Exception
     */
    @Test
    public void testPossibleEmpressMovesEmptyBoard() throws Exception {
        Coordinate rookPosition = new Coordinate(7, 0);

        //Set up a board with a special piece
        Board board = new Board();
        board.setPiecesOnRectangleBoard();
        board.removePiece(rookPosition);
        ArrayList<Piece> allPieces = board.getAllPiece();
        allPieces.add(new Empress(Piece.PieceColor.WHITE, rookPosition));

        //Remove all pieces except the empress
        for (int row = 0; row < board.getHorizontalSize(); row++) {
            for (int col = 0; col < board.getVerticalSize(); col++) {
                if (!(row == rookPosition.getRow() && col == rookPosition.getColumn())) {
                    board.removePiece(new Coordinate(row, col));
                }
            }
        }

        //Test for possible moves
        Piece empress = board.getPiece(rookPosition);
        ArrayList<Coordinate> al = empress.possibleMoves(board);
        assertEquals(16, al.size());
    }
}