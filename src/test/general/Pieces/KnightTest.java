package general.Pieces;

import general.Board;
import general.Coordinate;
import general.History;
import general.Piece;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class KnightTest {

    /**
     * Test possible moves of a knight in the original starting board
     *
     * @throws Exception
     */
    @Test
    public void testPossibleMoves() throws Exception {
        Board board = new Board();
        board.setPiecesOnRectangleBoard();
        Piece knight = board.getPiece(new Coordinate(7, 1));
        ArrayList<Coordinate> al = knight.possibleMoves(board);
        assertEquals(2, al.size());
    }

    /**
     * Test possible moves of a knight in the middle of a original starting board
     *
     * @throws Exception
     */
    @Test
    public void testPossibleKnightMovesCenterBoard() throws Exception {
        Board board = new Board();
        board.setPiecesOnRectangleBoard();
        Piece knight = board.getPiece(new Coordinate(7, 1));

        //Move the knight in the middle
        knight.setCoordinate(new Coordinate(3, 3));

        ArrayList<Coordinate> al = knight.possibleMoves(board);
        assertEquals(8, al.size());
    }

    /**
     * Test possible moves of a knight in the starting position of an empty board
     *
     * @throws Exception
     */
    @Test
    public void testPossibleKnightMovesEmptyBoard() throws Exception {
        Board board = new Board();
        History history = new History();
        board.setPiecesOnRectangleBoard();
        Piece knight = board.getPiece(new Coordinate(7, 1));

        //Remove all pieces except the knight
        for (int row = 0; row < board.getHorizontalSize(); row++) {
            for (int col = 0; col < board.getVerticalSize(); col++) {
                if (!(row == 7 && col == 1)) {
                    board.removePiece(new Coordinate(row, col));
                }
            }
        }

        ArrayList<Coordinate> al = knight.possibleMoves(board);
        assertEquals(3, al.size());
    }
}