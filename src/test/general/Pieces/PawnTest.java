package general.Pieces;

import general.Board;
import general.Coordinate;
import general.Piece;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class PawnTest {

    /**
     * Test possible moves of a pawn in the original starting board
     *
     * @throws Exception
     */
    @Test
    public void testPossibleMoves() throws Exception {
        Board board = new Board();
        board.setPiecesOnRectangleBoard();
        Piece bishop = board.getPiece(new Coordinate(6, 0));
        ArrayList<Coordinate> al = bishop.possibleMoves(board);
        assertEquals(2, al.size());
    }

    /**
     * Test possible moves of a pawn in the middle of a original starting board
     *
     * @throws Exception
     */
    @Test
    public void testPossiblePawnMovesCenterBoard() throws Exception {
        Board board = new Board();
        board.setPiecesOnRectangleBoard();
        Piece bishop = board.getPiece(new Coordinate(6, 0));

        //Move the bishop in the middle
        bishop.setCoordinate(new Coordinate(3, 3));

        ArrayList<Coordinate> al = bishop.possibleMoves(board);
        assertEquals(1, al.size());
    }

    /**
     * Test possible moves of a pawn that are able to capture enemy pieces
     *
     * @throws Exception
     */
    @Test
    public void testPossiblePawnMovesCapture() throws Exception {
        Board board = new Board();
        board.setPiecesOnRectangleBoard();
        Piece bishop = board.getPiece(new Coordinate(6, 0));

        //Move the bishop in the middle
        bishop.setCoordinate(new Coordinate(2, 1));

        ArrayList<Coordinate> al = bishop.possibleMoves(board);
        assertEquals(2, al.size());
    }
}